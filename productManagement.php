<?php
session_start();
require("productModel.php");

if (!isset($_SESSION['loginProfile'])) {
	//* if not logged in, redirect page to loginUI.php
	header("Location: loginUI.php");
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Basic HTML Examples</title>
</head>

<body>
	<p>產品管理頁面
		[<a href="logout.php">登出</a>]
	</p>
	<hr>
	<?php
	echo "您好 ", $_SESSION["loginProfile"]["uName"],
		", 您的 ID 是: ",
		$_SESSION["loginProfile"]["uID"],
    ", 您的身分是: ";
  if($_SESSION["loginProfile"]["uRole"] == 0)
    echo "會員<HR>";
  else
    echo " 員工<HR>";
	$result = getProductList();
	?>
  <br>
  <a href="orderStatus.php" target="_self">顯示訂單</a>
	<a href="productHandler.php?act=add" target="_self">新增產品</a>
	<br>
	<table width="200" border="1">
		<tr>
			<td>id</td>
			<td>名稱</td>
			<td>價格</td>
			<td>說明</td>
      <td>管理</td>
      <!-- <td>Delete</td> -->
		</tr>
		<?php
		while ($rs = mysqli_fetch_assoc($result)) {
			echo "<tr><td>" . $rs['prdID'] . "</td>";
			echo "<td>{$rs['name']}</td>";
			echo "<td>", $rs['price'], "</td>";
			echo "<td>", $rs['detail'], "</td>";
      echo "<td><a href='productHandler.php?act=manage&prdID=" . $rs['prdID'] . "' target='_self'>修改</a></td>";
			// echo "<td><a href='productHandler.php?act=delete&prdID=" . $rs['prdID'] . "' target='_self'>+</a></td>";
			echo "</tr>";
		}
		?>
	</table>

	<?php
	if (isset($_GET['act'])) {
		if ($_GET['act'] == 'addToCart') { ?>
			<script>
				var res = confirm("Add to cart successfully!");
			</script>
	<?php
		}
	}
	?>


</body>

</html>