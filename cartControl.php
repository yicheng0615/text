<?php
session_start();
require("cartModel.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Basic HTML Examples</title>
</head>

<body>
  <p>This is the cartControl page</p>
  <hr>
</body>
<?php
$act = $_GET['act'];
$uID = $_SESSION["loginProfile"]['uID'];

if ($act == "add") {
  $prdID = $_GET['prdID'];

  if (addCartItem($uID, $prdID) == false) {
    echo "error exception: fail to adding item\n";
  } else
    echo "add item successfully!\n";
    header("Location: mainUI.php?act=addToCart");
} elseif ($act == "remove") {
  $prdID = $_GET['prdID'];
  $quantity = $_GET['quantity'];

  if (removeCartItem($uID, $prdID, $quantity) == false)
    echo "error exception: fail to remove item\n";
  else
    echo "remove item successfully!\n";
} elseif ($act == "checkout") {
  $address = $_POST['ADDRESS'];

  // $status = $_POST['STATUS'];
  $status = 0;

  if (checkout($uID, $address) == false) {
    echo "error exception: fail to checkout\n";
  } else{
    echo "checkout successfully!\n";
    header("Location: mainUI.php?act=checkout");
  }
} else {
  echo "error exception:　error parameter 'act'";
}
?>
<br>
<a href="cartView.php" target="_self">Go to cart</a>
<a href="mainUI.php" target="_self">Back to main</a>

</html>