<?php
session_start();
require("orderModel.php");

if (!isset($_SESSION['loginProfile'])) {
	//* if not logged in, redirect page to loginUI.php
	header("Location: loginUI.php");
}

$selected = FALSE; //* record the status is set or not
$STATUS_NUM = 3; //* consant: use to record how much status
$SA = ["待寄出", "已寄出", "已送達"];

if (isset($_POST['selectedStatus'])) {
	$selected = TRUE;
	$result = getAccordingOrder($_POST['selectedStatus']);
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Basic HTML Examples</title>
</head>

<?php
if (isset($_GET['act'])) {
	if ($_GET['act'] == 'modifyStatus') { ?>
		<script>
			var res = confirm("Modify status successfully!");
		</script>
<?php
	}
}
?>


<body>
	<p>訂單狀態
		[<a href="logout.php">登出</a>]
	</p>
	<hr>
	<?php
	echo "您好 ", $_SESSION["loginProfile"]["uName"],
		", 您的ID是 : ",
		$_SESSION["loginProfile"]["uID"],
		", 您的身分是 : ";
	if ($_SESSION["loginProfile"]["uRole"] == 0)
		echo " 會員<HR>";
	else
		echo " 員工<HR>";
	"<HR>";
	?>


	<form method="post" action="orderStatus.php">
		選擇狀態: <select id="sel-status" name="selectedStatus">
			<?php
			if ($selected == TRUE) {
				for ($i = 1; $i <= $STATUS_NUM; $i++) {
					if ($i == $_POST['selectedStatus']) {
						echo "<option value='" . $i . "' selected>" . $SA[$i - 1] . "</option>";
					} else
						echo "<option value='" . $i . "' >" . $SA[$i - 1] . "</option>";
				}
			} else {
				echo "<option value='0' selected disabled>請選擇產品狀態</option>";
				echo "<option value='1'>待寄出</option>";
				echo "<option value='2'>已寄出</option>";
				echo "<option value='3'>已送達</option>";
			}
			?>
		</select>
		<input type="submit" value="確定">
	</form>

	<?php
	if ($selected == TRUE) {
		echo  "
		<div id='order-div'>
				
				<table width='850' border='1'>
						<tr>
							<td>訂單  ID</td>
							<td>uID</td>
							<td>日期</td>
							<td>狀態</td>
							<td>訂單細節</td>
							<td>修改狀態</td>
						</tr>";
		while ($rs = mysqli_fetch_assoc($result)) {
			echo '<tr><td>' . $rs['ordID'] . "</td>";
			echo '<td>' . $rs['uID'] . "</td>";
			echo "<td>{$rs['orderDate']}</td>";
			echo "<td>", $rs['status'], "</td>";
			/* echo "<td><form method='post' action='orderControl.php?act=modifyStatus&ordID={$rs['ordID']}'>
									modify status<select class='modify-status' name='status'>
										<option value='0' selected disabled>Please choose a status</option>
										<option value='1'>待寄出</option>
										<option value='2'>已寄出</option>
										<option value='3'>已送達</option>
									</select>
									<input type='submit' value='modify status' class='submit'>
								<form></td>"; */
			$tmp = "'block'"; ?>

			<!-- 
			//* more info button	
			<td>
				<button onclick="
					var ele = document.querySelector('#orderDetail-<?php echo $rs['ordID']; ?>'); ele.style.display='block'">more info
				</button>
			</td> -->
			<!-- //* more info panel -->
			<?php $detail = getOrderDetail($rs['ordID']); ?>
			<td>
				<div id='orderDetail-<?php echo $rs['ordID']; ?>' class='w3-panel w3-display-container' style=" margin: 0px; padding: 10px; display: block">
					<!-- <span onclick="this.parentElement.style.display='none'" class='w3-button w3-large w3-gray w3-display-topright' style="margin: 0px; padding:0px">close</span> -->
					<table border='1'>
						<tr>
							<td>產品 ID</td>
							<td>產品名稱</td>
							<td>數量</td>
							<td>價錢</td>
							<td>說明</td>
						</tr>
				<?php
				while ($ds = mysqli_fetch_assoc($detail)) {
					echo "<tr><td>" . $ds['prdID'] . "</td>";
					echo "<td>" . $ds['name'] . "</td>";
					echo "<td>", $ds['quantity'], "</td>";
					echo "<td>", $ds['price'], "</td>";
					echo "<td>", $ds['detail'], "</td>";
					echo "</tr>";
				}

				echo "</table>";
				echo "</div>";
				echo "</td>";
				echo "<td><a href='orderStatusHandler.php?act=modifyStatus&ordID={$rs['ordID']}&status={$rs['status']}'>修改</td>";
				echo "</tr>";
			}
			echo "</table>";
			echo "</div>";
		} else
			echo "<p>* 請選擇產品狀態並按下確定</p>";
				?>

				<a href=" productManagement.php" target="_self">回去產品管理介面</a>

</body>
<script>
	var selStatus = document.querySelector('#sel-status');
	var orderDiv = document.querySelector('#order-div');
	var modStatus = document.querySelectorAll('.modify-status');
	var submit = document.querySelectorAll('.submit');

	for (let i = 0; i < submit.length; i++) {
		submit[i].disabled = true;
	}

	setInterval(function() {
		for (let i = 0; i < modStatus.length; i++) {
			if (modStatus[i].value == 0) {
				submit[i].disabled = true;
			} else
				submit[i].disabled = false;
		}
	}, 1000);

	for (let i = 0; i < modStatus.length; i++) {
		modStatus[i].addEventListener('changed', function() {
			if (modStatus.value != 0) {
				submit.disabled = false;
			}
		});
	}
	/* selStatus.addEventListener('changed', function() {
		var element = getInnerHtml(selStatus.value);
		orderDiv.innerHTML = element;
	});
	
	function getInnerHtml(status) {
		
		<?php
		// $status = status;
		// $result = getAccordingOrder($status);

		?>
		
	} */
</script>

</html>