-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- 主機： 127.0.0.1
-- 產生時間： 
-- 伺服器版本： 10.4.6-MariaDB
-- PHP 版本： 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 資料庫： `test`
--

-- --------------------------------------------------------

--
-- 資料表結構 `cart`
--

CREATE TABLE `cart` (
  `cID` int(11) NOT NULL,
  `uID` varchar(11) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 傾印資料表的資料 `cart`
--

INSERT INTO `cart` (`cID`, `uID`) VALUES
(1, 'jc'),
(2, 'qwer'),
(3, 'admin'),
(4, 'user'),
(5, 'test123456'),
(6, 'y');

-- --------------------------------------------------------

--
-- 資料表結構 `cartitem`
--

CREATE TABLE `cartitem` (
  `serno` int(11) NOT NULL,
  `cID` int(11) NOT NULL,
  `prdID` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 傾印資料表的資料 `cartitem`
--

INSERT INTO `cartitem` (`serno`, `cID`, `prdID`, `quantity`) VALUES
(1, 1, 1, 2);

-- --------------------------------------------------------

--
-- 資料表結構 `orderitem`
--

CREATE TABLE `orderitem` (
  `serno` int(11) NOT NULL,
  `prdID` int(11) NOT NULL,
  `ordID` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 傾印資料表的資料 `orderitem`
--

INSERT INTO `orderitem` (`serno`, `prdID`, `ordID`, `quantity`, `price`) VALUES
(12, 1, 14, 3, 225000),
(13, 3, 14, 1, 5),
(14, 2, 14, 1, 0),
(16, 2, 16, 4, 160),
(17, 5, 17, 1, 3999),
(18, 2, 17, 1, 10),
(19, 3, 17, 1, 10),
(20, 5, 18, 1, 3999),
(21, 2, 18, 1, 10),
(22, 3, 18, 2, 40),
(23, 2, 19, 1, 10),
(24, 3, 20, 1, 10),
(25, 8, 21, 1, 999),
(26, 3, 22, 1, 10),
(27, 6, 23, 1, 2195),
(28, 8, 23, 1, 999),
(29, 5, 24, 1, 3999),
(30, 3, 24, 1, 10),
(31, 8, 25, 1, 999),
(32, 3, 25, 1, 10);

-- --------------------------------------------------------

--
-- 資料表結構 `product`
--

CREATE TABLE `product` (
  `prdID` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `detail` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 傾印資料表的資料 `product`
--

INSERT INTO `product` (`prdID`, `name`, `price`, `detail`) VALUES
(3, 'pencil', 10, 'useful'),
(5, 'jacket', 3999, 'warm'),
(6, 'G502', 2195, 'mouse'),
(8, 'watch', 999, 'casio');

-- --------------------------------------------------------

--
-- 資料表結構 `user`
--

CREATE TABLE `user` (
  `uID` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `role` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- 傾印資料表的資料 `user`
--

INSERT INTO `user` (`uID`, `password`, `name`, `role`) VALUES
('admin', '1111', 'w', 1),
('jc', '123', '', 0),
('qwer', '1111', 'lol', 1),
('test', '111', 'qq', 0),
('test123456', '123456', 'q', 0),
('user', '1111', 'test', 0),
('y', '1111', 'tt', 0);

-- --------------------------------------------------------

--
-- 資料表結構 `userorder`
--

CREATE TABLE `userorder` (
  `ordID` int(11) NOT NULL,
  `uID` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `orderDate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 傾印資料表的資料 `userorder`
--

INSERT INTO `userorder` (`ordID`, `uID`, `orderDate`, `address`, `status`) VALUES
(14, 'qwer', '2019-12-19 13:37:22', 'chin turtle', 3),
(16, 'jc', '2019-12-19 13:45:08', 'w', 3),
(17, 'jc', '2019-12-19 12:43:48', 'i don\'t know', 3),
(18, 'user', '2019-12-19 13:37:42', 'ncnu', 1),
(19, 'user', '2019-12-19 12:54:54', 'here', 2),
(20, 'test123456', '2019-12-18 06:23:35', 'wue', 2),
(21, 'y', '2019-12-18 06:23:45', 'home', 2),
(22, 'y', '2019-12-19 12:41:02', 'home', 3),
(23, 'test123456', '2019-12-19 12:55:23', 'm', 2),
(24, 'test123456', '2019-12-19 13:49:33', '268', 1),
(25, 'test123456', '2019-12-19 13:48:00', 'qqqqq', 1);

--
-- 已傾印資料表的索引
--

--
-- 資料表索引 `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`cID`);

--
-- 資料表索引 `cartitem`
--
ALTER TABLE `cartitem`
  ADD PRIMARY KEY (`serno`);

--
-- 資料表索引 `orderitem`
--
ALTER TABLE `orderitem`
  ADD PRIMARY KEY (`serno`);

--
-- 資料表索引 `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`prdID`);

--
-- 資料表索引 `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`uID`);

--
-- 資料表索引 `userorder`
--
ALTER TABLE `userorder`
  ADD PRIMARY KEY (`ordID`);

--
-- 在傾印的資料表使用自動遞增(AUTO_INCREMENT)
--

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `cart`
--
ALTER TABLE `cart`
  MODIFY `cID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `cartitem`
--
ALTER TABLE `cartitem`
  MODIFY `serno` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `orderitem`
--
ALTER TABLE `orderitem`
  MODIFY `serno` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `product`
--
ALTER TABLE `product`
  MODIFY `prdID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `userorder`
--
ALTER TABLE `userorder`
  MODIFY `ordID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
