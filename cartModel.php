<?php
require("dbconfig.php");

function getCartList($uID)
{
  global $db;
  $cID = getCartID($uID);
  $sql = "SELECT cartitem.prdID, cartitem.quantity, product.name, product.price, product.detail, cartitem.quantity * product.price AS `total price` 
          FROM cartitem, product 
          WHERE cartitem.cID = ? AND cartitem.prdID = product.prdID 
          ORDER BY product.price DESC ";
  $stmt = mysqli_prepare($db, $sql); //prepare sql statement
  mysqli_stmt_bind_param($stmt, "i", $cID); //bind parameters with variables
  mysqli_stmt_execute($stmt);  //執行SQL
  $result = mysqli_stmt_get_result($stmt); //get the results

  return $result;
}

function getCartID($uID)
{
  global $db;
  $sql = "SELECT cID FROM cart WHERE uID=? ";
  $stmt = mysqli_prepare($db, $sql); //prepare sql statement
  mysqli_stmt_bind_param($stmt, "s", $uID); //bind parameters with variables
  mysqli_stmt_execute($stmt);  //執行SQL
  $result = mysqli_stmt_get_result($stmt); //get the results

  $counter = 0;
  $cID = -1;

  while ($rs = mysqli_fetch_assoc($result)) {
    $counter++; // count how much cart this user have
    $cID = $rs['cID'];
  }
  if ($counter == 0) {
    createCart($uID); // create a personal cart
    $cID = getCartID($uID);
    return $cID;
  } elseif ($counter > 1) {
    echo "error exception: more than one cart<br>";
    while (true) { }
  } else
    return $cID;
}

function createCart($uID)
{
  global $db;
  $sql = "INSERT INTO cart (`cID`, `uID`) VALUE (NULL, ?)";
  $stmt = mysqli_prepare($db, $sql);
  mysqli_stmt_bind_param($stmt, "s", $uID);
  $result = mysqli_stmt_execute($stmt);
  // @debug
  if ($result == false) {
    echo "error exception when creating a new cart<br>";
    while (true) { }
  } else
    echo "create new cart successfully<br>";
  return;
}

function itemExisted($cID, $prdID)
{
  global $db;
  $sql = "SELECT `quantity` FROM cartitem WHERE cID=? AND prdID=?";
  $stmt = mysqli_prepare($db, $sql);
  mysqli_stmt_bind_param($stmt, "ii", $cID, $prdID);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);
  if ($rs = mysqli_fetch_assoc($result))
    return $rs['quantity']; // exist, then return current quantity
  return 0; // do not exist
}

function addCartItem($uID, $prdID)
{
  global $db;
  $cID = getCartID($uID);
  $quantity = itemExisted($cID, $prdID);
  // is not exist yet
  if ($quantity == 0) {
    $sql = "INSERT INTO cartitem (`serno`, `cID`, `prdID`, `quantity`) VALUES (NULL, ?, ?, 1)";
    $stmt = mysqli_prepare($db, $sql);
    mysqli_stmt_bind_param($stmt, "is", $cID, $prdID);
    $result = mysqli_stmt_execute($stmt);
  } else {
    $sql = "UPDATE `cartitem` SET `quantity` = ? WHERE `cID`=? AND `prdID`=?";
    $stmt = mysqli_prepare($db, $sql); //prepare sql statement
    $quantity = $quantity + 1;
    mysqli_stmt_bind_param($stmt, "iis", $quantity, $cID, $prdID);
    $result = mysqli_stmt_execute($stmt);
  }
  return $result;
}

function removeCartItem($uID, $prdID, $quantity)
{
  global $db;
  $cID = getCartID($uID);
  if ($quantity == 1) { // delete data
    $sql =  "DELETE FROM cartitem WHERE cID=? AND prdID=?";
    $stmt = mysqli_prepare($db, $sql);
    mysqli_stmt_bind_param($stmt, "ii", $cID, $prdID);
    $result = mysqli_stmt_execute($stmt);
  } else {
    $sql = "UPDATE cartitem SET quantity = ? WHERE cID=? AND prdID=?";
    $stmt = mysqli_prepare($db, $sql); //prepare sql statement
    $quantity = $quantity - 1;
    mysqli_stmt_bind_param($stmt, "iii", $quantity, $cID, $prdID);
    $result = mysqli_stmt_execute($stmt);
  }

  return $result;
}

function checkout($uID, $address)
{
  //* add a data in `userorder`, then get the `cartitem`, add the them into `orderitem` one by one
  global $db;
  require("orderModel.php");
  
  date_default_timezone_set("Asia/Taipei");
  $timestamp  = date('Y-m-d H:i:s');
  $ordID = addUserOrder($uID, $timestamp, $address, 1);
  if($ordID == -1)
    return false;
  else{
    //* create an data in `orderitem`
    $cartitems = getCartList($uID);
    while($item = mysqli_fetch_assoc($cartitems)){
      if(addOrderItem($ordID, $item['prdID'], $item['quantity'], $item['total price']) == false)
        return false;
      
        //* set quantity as 1, so that this item could be removed aompletely
      if(removeCartItem($uID, $item['prdID'], 1) == false) // true or false
        return false; 
    }
    return true;
  }
}

function getCartDetail($uID)
{
  return false;
}
