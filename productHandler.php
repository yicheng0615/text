<?php
session_start();
require("productModel.php");

/* if (!isset($_SESSION['loginProfile'])) {
  // if not logged in, redirect page to loginUI.php
	header("Location: loginUI.php");
} */

//* modify or remove
if ($_GET['act'] == "manage") {
  $act = 'manage';
  if (isset($_GET['prdID']))
    $prdID = $_GET['prdID'];
  else
    echo "ERROR EXCEPTION: agument 'prdID' is NULL!<HR>";
  $result = getProductDetail($prdID);
} else {
  $act = 'add';
}



?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Basic HTML Examples</title>
</head>

<body>
  <p>修改產品介面
    [<a href="logout.php">登出</a>]
  </p>
  <hr>
  <!-- form -->
  <p>產品細節</p>

  <!-- t1<input type="text" name="t1"> -->
  <?php
  if ($act == "manage") {
    if ($rs = mysqli_fetch_assoc($result)) {
      ?>
      <form method="post" action="productControl.php?act=<?php echo "modify&prdID=", $prdID; ?>" target="_self">
        產品 ID: <?php echo $rs['prdID']; ?> <br>
        產品名稱: <input type="text" name="name" value="<?php echo $rs['name']; ?>" required> <br>
        價格: <input type="number" min="0" step="1" name="price" value="<?php echo $rs['price']; ?>" required> <br>
        說明: <input type="text" name="detail" value="<?php echo $rs['detail']; ?>"> <br>
        <input type="button" value="刪除" id="del-btn">
        <input type="submit" value="確定">
      </form>
    <?php }
    } else { ?>
    <form method="post" action="productControl.php?act=add" target="_self">
      產品 ID:<input type="text" name="prdID" value="DB自動產生" disabled required> <br>
      產品名稱: <input type="text" name="name" required> <br>
      價格: <input type="number" min="0" step="1" name="price" placeholder="請輸入 >= 0" required> <br>
      說明: <input type="text" name="detail"> <br>
      <input type="submit" value="確定">
    </form>
  <?php } ?>
  <hr>
  <button id="cancel-btn">取消</button>

</body>

<script>
  var delBtn = document.querySelector('#del-btn');
  var cancelBtn = document.querySelector('#cancel-btn');

  cancelBtn.addEventListener('click', function() {
    if (confirm("Would back to PRODUCT MANAGEMENT\nR U sure?") == true)
      window.location.assign("productManagement.php");
  });
  <?php
  if ($act == "manage") {
    echo "delBtn.addEventListener('click', function() {
      if (confirm('R U sure U want to delete this product? ') == true)
        window.location.assign('productControl.php?act=remove&prdID=";
    echo $prdID, "');
    });";
  } ?>
</script>

</html>