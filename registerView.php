<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Basic HTML Examples</title>
</head>

<body>
  <p>This is a Pargraph</p>
  <hr>
  <!-- form -->
  <p>Register</p>
  <form method="post" action="registerControl.php" target="_self">
    ID: <input type="text" name="ID" required> <br>
    Password: <input type="password" name="PWD" required> <br>
    Name: <input type="text" name="NAME" required> <br>
    Role: <select name="ROLE">
      <option value="0">Member</option> <!-- 0: staff, 1: member -->
      <option value="1">Staff</option>
    </select>
    <input type="submit">
  </form>
  <hr>

</body>

</html>