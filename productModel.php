<?php
  require_once("dbconfig.php");
	
	function getProductList()
  {
	  global $db;
	  $sql = "SELECT * FROM product ORDER BY prdID";
	  $stmt = mysqli_prepare($db, $sql); //prepare sql statement
	  mysqli_stmt_execute($stmt);  //執行SQL
	  $result = mysqli_stmt_get_result($stmt); //get the results
	  return $result;
	}
	
	function getProductDetail($prdID){
		global $db;
	  $sql = "SELECT prdID, name, price, detail FROM product WHERE prdID=?";
	  $stmt = mysqli_prepare($db, $sql);
	  mysqli_stmt_bind_param($stmt, "i", $prdID);
	  mysqli_stmt_execute($stmt);
	  $result = mysqli_stmt_get_result($stmt);
	  return $result;
	}

	function removeProduct($prdID){
		global $db;
		$sql = "DELETE FROM `product` WHERE prdID=?";
		$stmt = mysqli_prepare($db, $sql);
		mysqli_stmt_bind_param($stmt, "i", $prdID);
		$result = mysqli_stmt_execute($stmt);
		return $result; 
	}

	function modifyProduct($prdID, $name, $price, $detail){
		global $db;
		$sql = "UPDATE `product` SET `name`=?, `price`=?, `detail`=? WHERE prdID=?";
		$stmt = mysqli_prepare($db, $sql);
		mysqli_stmt_bind_param($stmt, "sisi", $name, $price, $detail, $prdID);
		$result = mysqli_stmt_execute($stmt);
		return $result;
	}

	function addProduct($name, $price, $detail){
		global $db;
		$sql = "INSERT INTO `product` (`prdID`, `name`, `price`, `detail`) VALUES (NULL, ?, ?, ?)";
		$stmt = mysqli_prepare($db, $sql);
		mysqli_stmt_bind_param($stmt, "sis", $name, $price, $detail);
		$result = mysqli_stmt_execute($stmt);
		return $result; 
	}
?>