<?php
session_start();
require("orderModel.php");

if (!isset($_SESSION['loginProfile'])) {
	// if not logged in, redirect page to loginUI.php
	header("Location: loginUI.php");
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Basic HTML Examples</title>
</head>

<body>
	<p>追蹤訂單
		[<a href="logout.php">登出</a>]
	</p>
	<hr>
	<?php
	echo "您好  ", $_SESSION["loginProfile"]["uName"],
		", 您的 ID 是 : ",
		$_SESSION["loginProfile"]["uID"],
		", 您的身分是 : ";
	if ($_SESSION["loginProfile"]["uRole"] == 0)
		echo " 會員<HR>";
	else
		echo " 員工<HR>";

	$result = getOrderList($_SESSION["loginProfile"]['uID']);
	?>
	<table width="850" border="1">
		<tr>
			<td>訂單 ID</td>
			<td>日期</td>
			<td>狀態</td>
			<td>地址</td>
			<td>細節</td>
		</tr>
		<?php
		$SA = ["待寄出", "已寄出", "已送達"];
		while ($rs = mysqli_fetch_assoc($result)) {
			echo "<tr><td>" . $rs['ordID'] . "</td>";
			echo "<td>{$rs['orderDate']}</td>";
			echo "<td>", $SA[$rs['status']-1], "</td>";
			echo "<td>{$rs['address']}</td>";
			$tmp = "'block'"; ?>
			<td><button onclick="
						var ele = document.querySelector('#orderDetail-<?php echo $rs['ordID']; ?>');
						ele.style.display='block'">更多資訊
				</button>
			</td>
			<!-- //* more info panel -->
			<?php $detail = getOrderDetail($rs['ordID']); ?>
			<td>
				<div id='orderDetail-<?php echo $rs['ordID']; ?>' class='w3-panel w3-display-container' style="margin: 0px; padding: 10px; display: none">
					<span onclick="this.parentElement.style.display='none'" class='w3-button w3-large w3-gray w3-display-topright' style="margin: 0px; padding:0px">關閉</span>
					<table border='1'>
						<tr>
							<td>產品 ID</td>
							<td>產品名稱</td>
							<td>數量</td>
							<td>價錢</td>
							<td>說明</td>
						</tr>
					<?php
						while ($ds = mysqli_fetch_assoc($detail)) {
							echo "<tr><td>" . $ds['prdID'] . "</td>";
							echo "<td>" . $ds['name'] . "</td>";
							echo "<td>", $ds['quantity'], "</td>";
							echo "<td>", $ds['price'], "</td>";
							echo "<td>", $ds['detail'], "</td>";
							echo "</tr>";
						}
						echo "</table>";
						echo "</div></td>";
						echo "</tr>";
					}
					?>
					</table>
					<a href=" mainUI.php" target="_self">回到大買特買頁面~</a>


</body>

</html>