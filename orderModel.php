<?php
require_once("dbconfig.php");

function getOrderList($uID)
{
	global $db;
	$sql = "SELECT ordID, orderDate, status, address FROM userorder WHERE uID=? ";

	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "s", $uID); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
	$result = mysqli_stmt_get_result($stmt); //get the results
	return $result;
}

function addUserOrder($uID, $timestamp , $address, $status)
{
	//TODO: create an data in `userorder`
	global $db;
	$sql = "INSERT INTO userorder (ordID, uID, orderDate, address, status) VALUES (NULL, ?, ?, ?, ?)";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "sssi", $uID, $timestamp , $address, $status);
	mysqli_stmt_execute($stmt);

	//todo: search the data just created
	$counter = 0;
	$sql = "SELECT ordID FROM userorder WHERE uID=? AND orderDate=? AND address=? AND status=?";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "sssi", $uID, $timestamp , $address, $status);
	mysqli_stmt_execute($stmt);
	$result = mysqli_stmt_get_result($stmt);
	if ($result == false) {
		echo "error exception: could not find the data just added few seconds ago. <br>";
		return -1;
	}
	while ($rs = mysqli_fetch_assoc($result)) {
		$counter++;
		$ordID = $rs['ordID'];
	}
	if ($counter != 1) {
		echo "error exception: there's more than expected group of data when searching that was just added few seconds ago. <br>
					expected to be 1 group of data, but there's " . $counter;
		while (true) { }
		return $ordID;
	} else {
		return $ordID;
	}
}


/* function getCartID($uID)
{
	return 0;
}
 */
function addOrderItem($ordID, $prdID, $quantity, $singlePrice)
{
	global $db;
	$sql = "INSERT INTO orderitem (serno, prdID, ordID, quantity, price) VALUES (NULL, ?, ?, ?, ?)";
	$stmt = mysqli_prepare($db, $sql);
	$price = $quantity * $singlePrice;
	mysqli_stmt_bind_param($stmt, "sssi", $prdID, $ordID, $quantity, $price);

	$result = mysqli_stmt_execute($stmt);
	return $result;
	// return false;
}

function removeOrderItem($ordID, $prdID)
{
	return false;
}

/* function checkout($ordID, $prdID)
{
	return false;
} */

function getOrderDetail($ordID)
{
	global $db;
	$sql = "SELECT orderitem.prdID, product.name, orderitem.quantity, orderitem.price, product.detail 
					FROM orderitem, product 
					WHERE orderitem.ordID = ? AND orderitem.prdID = product.prdID";
	$stmt = mysqli_prepare($db, $sql);
	mysqli_stmt_bind_param($stmt, "i", $ordID);
	mysqli_stmt_execute($stmt);
	$result = mysqli_stmt_get_result($stmt);
	return $result;
}

function modifyStatus($status, $ordID){
	global $db;
	$sql = "UPDATE userorder SET status=? WHERE ordID=? ";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "ii", $status, $ordID); //bind parameters with variables
	$result = mysqli_stmt_execute($stmt);  //執行SQL
	// $result = mysqli_stmt_get_result($stmt); //get the results
	return $result;
}

function getAccordingOrder($status){
	global $db;
	$sql = "SELECT ordID, uID, orderDate, status FROM userorder WHERE status=? ";

	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "i", $status); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
	$result = mysqli_stmt_get_result($stmt); //get the results
	return $result;
}
function getOneOrder($ordID){
	global $db;
	$sql = "SELECT ordID, uID, orderDate, status FROM userorder WHERE ordID=? ";

	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "i", $ordID); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
	$result = mysqli_stmt_get_result($stmt); //get the results
	return $result;
}
