<?php
session_start();
require("productModel.php");

if (!isset($_SESSION['loginProfile'])) {
	// if not logged in, redirect page to loginUI.php
	header("Location: loginUI.php");
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Basic HTML Examples</title>
</head>

<body>
	<p>大買特買頁面
		[<a href="logout.php">登出(你難道不買了嗎?)</a>]
	</p>
	<hr>
	<?php
	echo "您好歡迎光臨 ", $_SESSION["loginProfile"]["uName"],
		", 您的 ID 是: ",
		$_SESSION["loginProfile"]["uID"],
		", 您的身分是 : ";
	if ($_SESSION["loginProfile"]["uRole"] == 0)
		echo " 會員<HR>";
	else
		echo " Staff<HR>";
	$result = getProductList();
	?>
	<br>
	<a href="orderView.php" target="_self">追蹤訂單~</a>
	<a href="cartView.php" target="_self">購物車~</a>
	<br>
	<table width="500" border="1">
		<tr>
			<td>產品 id</td>
			<td>產品名稱</td>
			<td>價格</td>
			<td>說明</td>
			<td>增加(減少)</td>
		</tr>
		<?php
		while ($rs = mysqli_fetch_assoc($result)) {
			echo "<tr><td>" . $rs['prdID'] . "</td>";
			echo "<td>{$rs['name']}</td>";
			echo "<td>", $rs['price'], "</td>";
			echo "<td>", $rs['detail'], "</td>";
			// echo "addToCart.php?prdID='" . $rs['prdID'] . "<br>";
			// echo "<td><a href='addToCart.php?prdID=" . $rs['prdID'] . "' target='_self'>Add</a></td>";
			echo "<td><a href='cartControl.php?act=add&prdID=" . $rs['prdID'] . "' target='_self'>+</a></td>";
			echo "</tr>";
		}
		?>
	</table>

	<script>
		function includeHTML() {
			var z, i, elmnt, file, xhttp;
			/*loop through a collection of all HTML elements:*/
			z = document.getElementsByTagName("*");
			for (i = 0; i < z.length; i++) {
				elmnt = z[i];
				/*search for elements with a certain atrribute:*/
				file = elmnt.getAttribute("w3-include-html");
				if (file) {
					/*make an HTTP request using the attribute value as the file name:*/
					xhttp = new XMLHttpRequest();
					xhttp.onreadystatechange = function() {
						if (this.readyState == 4) {
							if (this.status == 200) {
								elmnt.innerHTML = this.responseText;
							}
							if (this.status == 404) {
								elmnt.innerHTML = "Page not found.";
							}
							/*remove the attribute, and call this function once more:*/
							elmnt.removeAttribute("w3-include-html");
							includeHTML();
						}
					}
					xhttp.open("GET", file, true);
					xhttp.send();
					/*exit the function:*/
					return;
				}
			}
		};
	</script>


	<?php
	if (isset($_GET['act'])) {
		$act = $_GET['act'];
		if ($act == "addToCart") {
			echo "<script>alert('Add to Cart successfully!')</script>";
		} else if ($act == "checkout") {
			echo "<script>alert('Checkout successfully!')</script>";
		}
	}
	?>


</body>

</html>