<?php
session_start();
require("cartModel.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>購物車頁面</title>
</head>

<body>
  <p>購物車
    [<a href="logout.php">登出</a>]
  </p>
  <hr>
  
  <?php
  echo "Hello ", $_SESSION["loginProfile"]["uName"],
  ", Your ID is: ",
  $_SESSION["loginProfile"]["uID"],
  ", Your Role is: ";
  if($_SESSION["loginProfile"]["uRole"] == 0)
    echo " Member<HR>";
  else
    echo " Staff<HR>";

  $result = getCartList($_SESSION["loginProfile"]['uID']);
  ?>
  <table width="500" border="1">
    <tr>
      <td>產品ID</td>
      <td>產品名稱</td>
      <td>數量</td>
      <td>單價</td>
	  <td>說明</td>
      <td>總價格</td>
      <td>刪除</td>
    </tr>
    <?php
    $total = 0;
    while ($rs = mysqli_fetch_assoc($result)) {
      echo "<tr><td>" . $rs['prdID'] . "</td>";
      echo "<td>{$rs['name']}</td>";
      echo "<td>", $rs['quantity'], "</td>";
      echo "<td>", $rs['price'], "</td>";
	  echo "<td>", $rs['detail'], "</td>";
      echo "<td>", $rs['total price'], "</td>";
      // echo "<td><a href='deleteFromCart.php?pedID=" . $rs['prdID'] . "'>delete</a></td>";
      echo "<td><a href='cartControl.php?act=remove&prdID=" . $rs['prdID'] . "&quantity=" . $rs['quantity'] . "'>remove</a></td>";
      echo "</tr>";
      $total += $rs['total price'];
    }
    ?>
  </table>
  <?php
  echo "total amount: " . $total;
  ?>
  <br>
  <hr>
  <p>Express Data</p>
  <form method="post" action="cartControl.php?act=checkout" target="_self">
    地址:<input type="text" name="ADDRESS" required> <br>
    <input type="submit" value="checkout">
  </form>


  <!-- <a href='cartControl.php?act=checkout'>checkout</a> -->
  <br>
  <a href="mainUI.php" target="_self">回到主頁面</a>


</body>

</html>