<?php
session_start();
require("orderModel.php");

/* if (!isset($_SESSION['loginProfile'])) {
  // if not logged in, redirect page to loginUI.php
  header("Location: loginUI.php");
} */

$ordID = $_GET['ordID'];
$status = $_GET['status'];
$result = getOneOrder($ordID);
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Basic HTML Examples</title>
</head>

<body>
  <p>修改訂單狀態
    [<a href="logout.php">登出</a>]
  </p>
  <hr>
  <!-- form -->
  <p>訂單細節</p>

  <!-- t1<input type="text" name="t1"> -->
  <?php
  if ($rs = mysqli_fetch_assoc($result)) {
  ?>
    <form method="post" action="orderControl.php?act=modifyStatus&ordID=<?php echo $rs['ordID']; ?>" target="_self">
      訂單 ID: <?php echo $rs['ordID']; ?> <br>
      uID: <?php echo $rs['uID']; ?> <br>
      日期: <?php echo $rs['orderDate']; ?> <br>
      狀態: <select class='modify-status' name='status'>
        <?php
        $SA = ["待寄出","已寄出","已送達"];
        for($i=0; $i<3; $i++){
          $tmp = $i + 1;
          if($i+1 == $status){
            echo "<option value='{$tmp}' selected>{$SA[$i]}</option>";
          }
          else
            echo "<option value='{$tmp}' >{$SA[$i]}</option>";
        }
        ?>
      </select>
      <input type="submit" value="修改">
    </form>
  <?php } ?>
  </form>
  <hr>  
  <a href="orderStatus.php">取消</a>

</body>